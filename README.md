# JQFramework
前言
===
JQFramework：有效提高开发效率，一些网络，扩展，分类，uikit下的封装！ 

---

### 更新记录:

###### V1.0.6
1. UIKit 新增`UIImage+JQSubImage.h` ;
2. UIKit 新增 `UIImage+JQGIF.h`Gif生成UIImage 方法;

###### V1.0.5
UIKit 新增: `UIControl+JQ.h`

###### V1.0.4
UIKit 新增 `NSString+JQAdd.h`

###### V1.0.3
容错版本, 优化大量代码, 新增 Network 网络库;

###### V1.0.2
基础版本提交, 有效提高开发效率，一些网络，扩展，分类，uikit下的封装！

---

### 目录:
| JQFramework |
| ----------- |
|     ![](https://github.com/xiaohange/JQFramework/blob/master/1.0.2.png?raw=true)        |
   


其中Nework下的 [JQHttpRequest](https://github.com/xiaohange/JQHttpRequest) 是本人的开源网络封装库, 可灵活使用;

---

## Installation

#### From CocoaPods

```
pod  "JQFramework"
```

#### Manually 
- Drag all source files under floder `JQFramework` to your project.

## Usage

引入头文件: `#import "JQFramework.h"` 其他自行按需使用即可;

## Other
[JQTumblrHud-高仿Tumblr App 加载指示器hud](https://github.com/xiaohange/JQTumblrHud)

[JQScrollNumberLabel：仿tumblr热度滚动数字条数](https://github.com/xiaohange/JQScrollNumberLabel)

[TumblrLikeAnimView-仿Tumblr点赞动画效果](https://github.com/xiaohange/TumblrLikeAnimView)

[JQMenuPopView-仿Tumblr弹出视图发音频、视频、图片、文字的视图](https://github.com/xiaohange/JQMenuPopView)

## Star

![微信公众号](http://blog26.com/images/wechatscan.gif)

微信公众号每周推送新技术, [CSDN博客](http://blog.csdn.net/qq_31810357)欢迎关注交流!
 
iOS开发者交流群：446310206 喜欢就❤️❤️❤️star一下吧！你的支持是我更新的动力！ Love is every every every star! Your support is my renewed motivation!


## License

This code is distributed under the terms and conditions of the [MIT license](LICENSE).